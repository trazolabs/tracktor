package tracktor

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"time"

	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
)

type Task struct {
	UUID uuid.UUID `gorm:"type:uuid;primary_key"`

	CreatedAt time.Time

	Queue   string
	RunAt   time.Time
	Payload MapField `gorm:"type:jsonb"`
}

func (task *Task) BeforeCreate(scope *gorm.Scope) error {
	task.UUID = uuid.NewV4()
	return nil
}

type MapField map[string]interface{}

func (f MapField) Value() (driver.Value, error) {
	return json.Marshal(f)
}

func (f *MapField) Scan(src interface{}) error {
	source, ok := src.([]byte)
	if !ok {
		return errors.New("type assertion .([]byte) failed")
	}

	return json.Unmarshal(source, f)
}
