package tracktor

import (
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
	"gitlab.com/trazolabs/wamqp"
)

func StartSubscriptionBackground(client *wamqp.Client, db *gorm.DB) {
	go func() {
		if err := client.SubscribeStruct("tracktor", func(command *AddTaskCommand) error {
			task := &Task{
				Queue:   command.Queue,
				RunAt:   command.RunAt.UTC(),
				Payload: command.Payload,
			}

			if err := db.Create(task).Error; err != nil {
				return err
			}

			logrus.WithFields(logrus.Fields{
				"task":  task.UUID,
				"runAt": task.RunAt,
			}).Info("task created")

			return nil
		}); err != nil {
			panic(err)
		}
	}()
}
