package tracktor

type EnvConfig struct {
	DatabaseDialect string `env:"DATABASE_DIALECT,default=sqlite3"`
	DatabaseArgs    string `env:"DATABASE_ARGS,default=db.sqlite3"`
	DatabaseLogMode bool   `env:"DATABASE_LOG_MODE,default=false"`

	RabbitMQURI string `env:"RABBITMQ_URI,default=amqp://guest:guest@localhost:5672"`

	LogLevel string `env:"DATABASE_LOG_MODE,default=info"`
}

var Env = &EnvConfig{}
