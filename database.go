package tracktor

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/sirupsen/logrus"
)

func CreateDatabase() *gorm.DB {
	db := createDatabase()
	db.LogMode(Env.DatabaseLogMode)

	// patch gorm callback.
	gorm.DefaultCallback.Create().Replace("gorm:force_reload_after_create", forceReloadAfterCreateCallback)

	gorm.AddNamingStrategy(&gorm.NamingStrategy{
		Column: func(name string) string {
			return name
		},
		Table: func(name string) string {
			return name
		},
	})

	db.AutoMigrate(&Task{})

	return db
}

func createDatabase() *gorm.DB {
	db, err := gorm.Open(Env.DatabaseDialect, Env.DatabaseArgs)
	if err != nil {
		logrus.Error(err)
		logrus.Fatal("failed to connect database")
	}

	if Env.DatabaseDialect == "postgres" {
		db.Exec(`CREATE EXTENSION IF NOT EXISTS "pg_trgm"`)
		db.Exec(`CREATE EXTENSION IF NOT EXISTS "uuid-ossp"`)
		db.Exec(`CREATE EXTENSION IF NOT EXISTS "unaccent"`)
	}

	return db
}

func forceReloadAfterCreateCallback(scope *gorm.Scope) {
	if blankColumnsWithDefaultValue, ok := scope.InstanceGet("gorm:blank_columns_with_default_value"); ok {
		db := scope.DB().New().Table(scope.TableName()).Select(blankColumnsWithDefaultValue.([]string))
		for _, field := range scope.Fields() {
			if field.IsPrimaryKey && !field.IsBlank {
				db = db.Where(fmt.Sprintf("%v = ?", scope.Quote(field.DBName)), field.Field.Interface())
			}
		}
		db.Scan(scope.Value)
	}
}
