module gitlab.com/trazolabs/tracktor

go 1.14

require (
	github.com/jinzhu/gorm v1.9.16
	github.com/joeshaw/envdecode v0.0.0-20190604014844-d6d9849fcc2c
	github.com/mattn/go-sqlite3 v2.0.1+incompatible // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.4.0 // indirect
	gitlab.com/trazolabs/wamqp v1.2.0
	golang.org/x/sys v0.0.0-20210616094352-59db8d763f22 // indirect
)
