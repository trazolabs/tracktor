package main

import (
	"time"

	"gitlab.com/trazolabs/tracktor"
	"gitlab.com/trazolabs/wamqp"
)

type MyCommand struct {
	Value string
}

func main() {
	client := wamqp.New("amqp://guest:guest@localhost:5672")

	if err := tracktor.Add(client, MyCommand{
		Value: "hello, world!",
	}, time.Now().Add(10*time.Second)); err != nil {
		panic(err)
	}
}
