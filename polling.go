package tracktor

import (
	"time"

	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
	"gitlab.com/trazolabs/wamqp"
)

func pollTasks(db *gorm.DB, client *wamqp.Client) error {
	tasks := make([]*Task, 0)
	if err := db.Find(&tasks, `"RunAt" <= ?`, time.Now().UTC()).Error; err != nil {
		return err
	}

	for _, task := range tasks {
		if err := db.Transaction(func(db *gorm.DB) error {
			if err := db.Delete(&Task{}, `"UUID" = ?`, task.UUID).Error; err != nil {
				return err
			}

			if err := client.Publish(task.Queue, task.Payload); err != nil {
				return err
			}

			return nil
		}); err != nil {
			return err
		}

		logrus.WithFields(logrus.Fields{
			"task":  task.UUID,
			"queue": task.Queue,
		}).Info("published a task")
	}

	return nil
}

func StartPolling(client *wamqp.Client, db *gorm.DB) {
	for {
		if err := pollTasks(db, client); err != nil {
			// if polling failed, we'll retry after 10 seconds.
			logrus.WithError(err).Warning("polling failed")
			time.Sleep(10 * time.Second)
			continue
		}
		time.Sleep(1 * time.Second)
	}
}
