package main

import (
	"github.com/joeshaw/envdecode"
	"github.com/sirupsen/logrus"
	"gitlab.com/trazolabs/tracktor"
	"gitlab.com/trazolabs/wamqp"
)

func main() {
	if err := envdecode.StrictDecode(tracktor.Env); err != nil {
		logrus.Fatal(err)
	}

	initLogger()

	var (
		db     = tracktor.CreateDatabase()
		client = wamqp.New(tracktor.Env.RabbitMQURI)
	)

	tracktor.StartSubscriptionBackground(client, db)
	tracktor.StartPolling(client, db)
}

func initLogger() {
	logLevel, err := logrus.ParseLevel(tracktor.Env.LogLevel)
	if err != nil {
		logrus.Fatal(err)
	}

	logrus.SetLevel(logLevel)
	logrus.SetFormatter(&logrus.JSONFormatter{})
}
