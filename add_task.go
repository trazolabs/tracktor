package tracktor

import (
	"encoding/json"
	"reflect"
	"time"

	"gitlab.com/trazolabs/wamqp"
)

type AddTaskCommand struct {
	Queue   string
	RunAt   time.Time
	Payload map[string]interface{}
}

func Add(client *wamqp.Client, task interface{}, runAt time.Time) error {
	queue := getStructName(task)

	// transform the task into a map[string]interface{} using JSON encoding.
	payload := make(map[string]interface{})
	data, err := json.Marshal(task)
	if err != nil {
		return err
	}

	if err := json.Unmarshal(data, &payload); err != nil {
		return err
	}

	// finally publish the command.
	return client.PublishStruct(AddTaskCommand{
		Queue:   queue,
		RunAt:   runAt,
		Payload: payload,
	})
}

func getStructName(v interface{}) string {
	if t := reflect.TypeOf(v); t.Kind() == reflect.Ptr {
		return t.Elem().Name()
	} else {
		return t.Name()
	}
}
